import React, { Component } from 'react';
import Link from '../../utils/ActiveLink';

class Navbar extends Component {
    // Navbar 
    _isMounted = false;
    state = {
        display: false,
        collapsed: true
    };
    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }
    componentDidMount() {
        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });
        window.scrollTo(0, 0);
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {

        const { collapsed } = this.state;
        const classOne = collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
        const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

        return (
            <React.Fragment>
                <div id="navbar" className="navbar-area">
                    <div className="main-nav">
                        <div className="container">
                            <nav className="navbar navbar-expand-md navbar-light">
                                <Link href="/">
                                    <a className="navbar-brand" style={{maxWidth:175}}>
                                        <img src={require("../../images/logo-white.png")} className="white-logo" alt="logo" />
                                        <img src={require("../../images/logo-black.png")} className="black-logo" alt="logo" />
                                    </a>
                                </Link>

                                <button 
                                    onClick={this.toggleNavbar} 
                                    className={classTwo}
                                    type="button" 
                                    data-toggle="collapse" 
                                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
                                    aria-expanded="false" 
                                    aria-label="Toggle navigation"
                                >
                                    <span className="navbar-toggler-icon"></span>
                                </button>

                                <div className={classOne} id="navbarSupportedContent">
                                    <ul className="navbar-nav">
                                        <li className="nav-item">
                                            <Link href="/" activeClassName="active">
                                                <a className="nav-link">Home</a>
                                            </Link>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="/about-us" activeClassName="active">
                                                <a className="nav-link">Sobre nós</a>
                                            </Link>
                                        </li>
 
                                        <li className="nav-item">
                                            <Link href="#">
                                                <a className="nav-link">
                                                    Soluções <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>

                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <Link href="/scora-demmand" activeClassName="active">
                                                        <a className="nav-link">Scora Demmand</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/scora-maintenance" activeClassName="active">
                                                        <a className="nav-link">Scora Maintenance</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/scora-risk" activeClassName="active">
                                                        <a className="nav-link">Scora Risk</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/scora-leads" activeClassName="active">
                                                        <a className="nav-link">Scora Leads</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/scora-journey" activeClassName="active">
                                                        <a className="nav-link">Scora Journey</a>
                                                    </Link>
                                                </li>

                                            </ul>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="#">
                                                <a className="nav-link">
                                                    Aplicações <i className="fas fa-chevron-down"></i>
                                                </a>
                                            </Link>

                                            <ul className="dropdown-menu">
                                                <li className="nav-item">
                                                    <Link href="/previsao-demanda" activeClassName="active">
                                                        <a className="nav-link">Previsão de demanda</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/manutencao-preditiva" activeClassName="active">
                                                        <a className="nav-link">Manutenção preditiva</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/analise-risco" activeClassName="active">
                                                        <a className="nav-link">Análise de risco</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/classificacao-leads" activeClassName="active">
                                                        <a className="nav-link">Classificação de leads</a>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link href="/jornada-cliente" activeClassName="active">
                                                        <a className="nav-link">Jornada do cliente</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>

                                        <li className="nav-item">
                                            <Link href="/contact" activeClassName="active">
                                                <a className="nav-link">Contato</a>
                                            </Link>
                                        </li>
                                    </ul>

                                    <div className="others-options">
                                        <Link href="/contact">
                                            <a className="default-btn">
                                                Solicite uma proposta 
                                                <span></span>
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Navbar;